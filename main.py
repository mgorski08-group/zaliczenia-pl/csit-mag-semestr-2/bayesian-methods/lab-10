import arviz as az
import matplotlib.pyplot as plt
import numpy as np
import pymc3 as pm

# A family has solar panels installed on the roof
# of their house. They noticed one day that one of the panels
# was defective, which reduced the generated power.
# Data series X is how many hours there was sun that day,
# while Y is how much energy was gathered over that day.

NUMBER_OF_DAYS = 30
DAY_OF_CHANGE = 10


def lin_rel(x, slope, intercept):
    return slope * x + intercept + np.random.normal(0, 0.3, len(x)) * x


def generate_x(n):
    return np.random.normal(5, 0.5, n)


def generate_y(x, switch_point):
    switch_len = 2  # must be even
    slope = np.concatenate([
        np.array([5] * (switch_point - switch_len // 2)),
        np.linspace(5, 4, switch_len),
        np.array([4] * (len(x) - switch_point - switch_len // 2)),
    ])
    np.array([5] * switch_point)
    intercept = 0

    y = lin_rel(x, slope, intercept)

    noise = np.random.normal(loc=0, scale=0.1, size=len(y))
    y += noise
    return y


x = generate_x(NUMBER_OF_DAYS)
y = generate_y(x, DAY_OF_CHANGE)

fig, ax = plt.subplots(nrows=2, ncols=1)
ax[0].scatter(range(DAY_OF_CHANGE), x[:DAY_OF_CHANGE], color="r", alpha=0.5)
ax[0].scatter(range(DAY_OF_CHANGE, NUMBER_OF_DAYS), x[DAY_OF_CHANGE:], color="g", alpha=0.5)

ax[1].scatter(range(DAY_OF_CHANGE), y[:DAY_OF_CHANGE], color="r", alpha=0.5)
ax[1].scatter(range(DAY_OF_CHANGE, NUMBER_OF_DAYS), y[DAY_OF_CHANGE:], color="g", alpha=0.5)

plt.show()

plt.scatter(x, y)
plt.show()

with pm.Model() as model:
    b_1 = pm.Uniform("b_1", lower=-10, upper=10)
    b_2 = pm.Uniform("b_2", lower=-10, upper=10)
    tau = pm.DiscreteUniform("tau", lower=0, upper=len(x) - 1)
    idx = np.arange(len(x))
    b = pm.math.switch(tau > idx, b_1, b_2)
    m_y = pm.Deterministic("m_y", b * x)
    sigma_y = pm.Uniform("sigma_y", lower=0.1, upper=10)
    energy = pm.Normal("energy", mu=m_y, sigma=sigma_y, observed=y)

    idata = pm.sample(2000, tune=2500, return_inferencedata=True)

az.plot_posterior(idata, var_names=['tau', 'b_1', 'b_2'])
plt.gcf().suptitle(f"")
plt.tight_layout()
plt.show()

pred_slope_1 = idata.posterior['b_1'].mean().item()
pred_slope_2 = idata.posterior['b_2'].mean().item()
pred_tau = int(idata.posterior['tau'].mean().item())

colors = ["r"] * pred_tau + ["g"] * (NUMBER_OF_DAYS - pred_tau)
plt.scatter(x, y, c=colors, alpha=0.5)

x_1 = np.array([np.min(x[:pred_tau]), np.max(x[:pred_tau])])
x_2 = np.array([np.min(x[pred_tau:]), np.max(x[pred_tau:])])
y_1 = x_1 * pred_slope_1
y_2 = x_2 * pred_slope_2
plt.plot(x_1, y_1, color='red')
plt.plot(x_2, y_2, color='green')

plt.show()
